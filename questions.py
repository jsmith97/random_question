import secrets
import click
import sys


def load_question_list(file_path) -> list:

    with open(file_path) as f:
        question_list = f.read().splitlines()
    return question_list


def write_to_file(data_to_write):
    if data_to_write:
        with open("asked_questions.txt", 'a+') as f:
            f.write(f"{data_to_write}\n")
    else:
        with open("asked_questions.txt", 'a+') as f:
            pass


def random_question(questions) -> str:
    if questions:
        question = secrets.choice(questions)
        return question
    else:
        print("there are no more questions, goodbye")
        sys.exit(0)


def prepare_questions():
    all_questions = load_question_list("couples_quiz.txt")
    asked_questions = load_question_list("asked_questions.txt")
    new_questions = list(set(all_questions) - set(asked_questions))
    return new_questions


def ask_the_user(new_questions):
    cont = True
    while cont:
        output = random_question(new_questions)
        new_questions.remove(output)
        write_to_file(output)
        print("\033[H\033[J")
        print(f"{output}\n")
        print(f"there are {len(new_questions)} questions remaining\n")

        if click.confirm('should I ask another question??', default=True):
            cont = True
        else:
            print("goodbye")
            sys.exit(0)


if __name__ == "__main__":
    write_to_file(None)
    new_questions = prepare_questions()
    ask_the_user(new_questions)
